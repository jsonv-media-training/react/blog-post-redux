import React, { FunctionComponent } from 'react';
import { Provider } from 'react-redux';

import './App.scss';
import PostList from '../components/post-list/PostList';
import store from '../redux/store';

const App: FunctionComponent<any> = () => {
  return (
    <Provider store={store}>
      <div className='container-md py-4'>
        <PostList/>
      </div>
    </Provider>
  );
}

export default App;
