import React, { Fragment, FunctionComponent, useEffect } from 'react';
import { connect } from 'react-redux';
import { Subscription } from 'rxjs';
import { ThunkDispatch } from 'redux-thunk';
import { motion } from 'framer-motion';

import './PostList.scss';
import { Action, AppState, Post } from '../../models';
import { PostListProps } from './props.model';
import { getPosts } from '../../redux/post/post.actions';
import { container, item } from './variants.framer-motion';
import UserProfile from '../user-profile/UserProfile';
import SkeletonElement from '../skeleton/SkeletonElement';
import Shimmer from '../skeleton/Shimmer';

const PostList: FunctionComponent<PostListProps> = ({ getPosts, posts }) => {

  useEffect(() => {
    const subscription: Subscription = getPosts();
    return () => subscription.unsubscribe()
  }, [getPosts])

  return (
    <Fragment>
      {
        posts.loading &&
        <ul className="list-group result">
          <p>Loading...</p>
          {
            Array.from(Array(5).keys()).map((val) => {
              return (
                <li
                  key={val}
                  className="list-group-item">
                  <div className="user">
                    <SkeletonElement type={'avatar'} style={{width: 75, height: 75}}/>
                    <SkeletonElement type={'text'} style={{width: 80, marginTop: 0}}/>
                  </div>
                  <div className="content">
                    <SkeletonElement type={'text'}/>
                    <SkeletonElement type={'text'}/>
                    <SkeletonElement type={'text'}/>
                    <SkeletonElement type={'text'}/>
                  </div>
                  <Shimmer/>
                </li>
              )
            })
          }
        </ul>
      }
      {
        !posts.loading && posts.data.length &&
        <motion.ul
          variants={container} initial="init" animate="show"
          className="list-group result">
          {
            !posts.loading && posts.data.map(post => {
              return (
                <motion.li
                  variants={item} whileHover={{ scale: 1.02 }} whileTap={{ scale: 0.95 }}
                  key={post.id}
                  className="list-group-item">
                  <UserProfile userId={post.userId}/>
                  <div className="content">
                    <div className="title">
                      {post.title}
                    </div>
                    <div className="body">
                      {post.body}
                    </div>
                  </div>
                </motion.li>
              )
            })
          }
        </motion.ul>
      }
    </Fragment>
  );
}

const mapStateToProps = (state: AppState) => ({
  posts: state.posts
});

const mapDispatchToProps = (dispatch: ThunkDispatch<Subscription, any, Action<Post[]>>) => ({
  getPosts: () => dispatch(getPosts())
});

export default connect(mapStateToProps, mapDispatchToProps)(PostList);