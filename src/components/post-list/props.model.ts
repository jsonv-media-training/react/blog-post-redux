import { Subscription } from 'rxjs';
import { Post, State } from '../../models';

export interface PropsModel {
  posts: State<Post[]>
}

export interface DispatchProps {
  getPosts: () => Subscription
}

export interface PostListProps extends PropsModel, DispatchProps {}
