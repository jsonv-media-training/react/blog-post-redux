import { Variants } from 'framer-motion';

export const container: Variants = {
  init: {
  },
  show: {
    transition: { staggerChildren: 0.07, delayChildren: 0.2 }
  }
}

export const item: Variants = {
  init: {
    opacity: 0,
    rotateX: -90
  },
  show: {
    opacity: 1,
    rotateX: 0,
    transition: {
      ease: [0.34, 1.56, 0.64, 1],
      duration: .8
    }
  }
};