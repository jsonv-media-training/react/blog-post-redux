import React, { FunctionComponent } from 'react';
import './Skeleton.scss';

const Shimmer: FunctionComponent<any> = ({}) => {
  return (
    <div className="shimmer-wrapper">
      <div className="shimmer"/>
    </div>
  )
}

export default Shimmer;