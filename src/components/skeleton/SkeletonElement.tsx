import React, { CSSProperties, FunctionComponent } from 'react';
import './Skeleton.scss';

const SkeletonElement: FunctionComponent<{ type: string, style?: CSSProperties }> = ({ type, style }) => {
  return (
    <div className={`skeleton ${type}`} style={{...style}}/>
  )
}

export default SkeletonElement