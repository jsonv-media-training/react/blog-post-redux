import React, { FunctionComponent } from 'react';
import { connect } from 'react-redux';

import './UserProfile.scss';
import { UserProfileProps } from './props.model';
import { AppState } from '../../models';


const UserProfile: FunctionComponent<UserProfileProps> = ({ user }) => {
  return (
    <div className="user">
      <img src={user?.profile} alt="user-img"/>
      <p className="font-monospace mx-0 mt-2 mb-0">{user?.username}</p>
    </div>
  )
}

const mapStateToProps = (state: AppState, ownProps: { userId: number }) => ({
  user: state.users.data.find(user => user.id === ownProps.userId)
});

export default connect(mapStateToProps, null)(UserProfile);