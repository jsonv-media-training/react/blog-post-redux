import { User } from '../../models';

export interface PropsModel {
  user?: User
}

export interface DispatchProps {}

export interface UserProfileProps extends PropsModel, DispatchProps {}