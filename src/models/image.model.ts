export interface Image {
  id: string,
  description: string,
  alt_description: string,
  height: number,
  width: number,
  created_at: Date,
  urls: {
    full: string,
    raw: string,
    regular: string,
    small: string,
    thumb: string
  }
}
export interface UnslashSearch {
  results: Image[]
  total: number
  total_pages: number
}
