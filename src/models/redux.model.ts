import { User } from './user.model';
import { Post } from './post.model';

export interface Action<T> {
  type: string;
  payload?: T | string | null
}

export interface State<T> {
  data: T,
  loading?: boolean;
  error?: string
}

export interface AppState {
  users: State<User[]>;
  posts: State<Post[]>;
}