import { ThunkAction, ThunkDispatch } from 'redux-thunk';
import { concatMap, delay, map, tap } from 'rxjs/operators';
import { forkJoin, Subscription } from 'rxjs';

import * as service from '../../services';
import { GET_POSTS } from './post.type';
import { Action, Image, Post, User } from '../../models';
import { successGetUser } from '../user/user.actions';


export const beginGetPosts= (): Action<any> => {
  return {
    type: GET_POSTS.START_REQUEST
  }
};

export const successGetPosts = (posts: Post[]): Action<Post[]> => {
  return {
    type: GET_POSTS.REQUEST_SUCCESS,
    payload: posts
  }
};

export const failedGetPosts = (error: any): Action<any> => {
  return {
    type: GET_POSTS.REQUEST_FAILURE,
    payload: error
  }
};

export const getPosts = (): ThunkAction<Subscription, any, null, Action<Post[]>> => {
  return (dispatch: ThunkDispatch<any, null, Action<Post[] | User>>): Subscription => {
    return service.getPosts()
      .pipe(
        tap(() => dispatch(beginGetPosts())),
        concatMap(posts => {
          const users$ = Array
            .from(new Set(posts.map(post => post.userId)))
            .map(userId => service.getUser(userId).pipe(
              map(user => { if (user) dispatch(successGetUser(user)); return user})
            ));

          return forkJoin([...users$,service.getImages(users$.length)])
            .pipe(map(res => {
              const images = res.pop() as Image[];
              const users = res.map((user, index) => {
                (user as User).profile = images[index].urls.thumb;
                return user;
              });
              return { posts, users }
            }));
        }),
        delay(2000)
      ).subscribe(
        res => dispatch(successGetPosts(res.posts)),
        err => dispatch(failedGetPosts(err.message)),
      );
  }
}

// type ThunkAction<R, S, E, A extends Action> = (
//   dispatch: ThunkDispatch<S, E, A>,
//   getState: () => S,
//   extraArgument: E
// ) => R;
// S = is the type of root state
//   = is the return type of the getState() method.
//
// E = is the type of the extra arguments passed to the ThunkAction
//
// A = is the action type defined in your application.
//   = it should be able to extend from Action.
//     (this means that it should be an object
//     that must have a `type` field.) Action type is defined in the redux typings.

