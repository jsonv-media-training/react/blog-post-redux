import { GET_POSTS } from './post.type';
import { Action, Post, State } from '../../models';

const initState: State<Post[]> = {
  data: [],
  loading: false,
  error: ''
}

export const postsReducer = (state = initState, action: Action<Post[]>): State<Post[]> => {
  switch (action.type) {
    case GET_POSTS.START_REQUEST:
      return {
        ...state,
        loading: true
      };
    case GET_POSTS.REQUEST_SUCCESS:
      return {
        loading: false,
        data: action.payload as Post[],
        error: ''
      };
    case GET_POSTS.REQUEST_FAILURE:
      return {
        loading: false,
        data: [],
        error: action.payload as string
      };
    default:
      return state;
  }
}