import { applyMiddleware, combineReducers, createStore } from 'redux';
import thunk from 'redux-thunk';

import { postsReducer } from './post/post.reducers';
import { AppState } from '../models';
import { composeWithDevTools } from 'redux-devtools-extension';
import { usersReducer } from './user/user.reducers';

//  Reducer<CombinedState<{posts: State<Post[]>, users: any[]}>>
const reducers =  combineReducers<AppState>({
  posts: postsReducer,
  users: usersReducer
});

export default createStore(
  reducers,
  composeWithDevTools(applyMiddleware(thunk))
);


