import { Action, User } from '../../models';
import { GET_USER } from './user.type';

export const successGetUser = (user: User): Action<User> => {
  return {
    type: GET_USER.REQUEST_SUCCESS,
    payload: user
  }
};
