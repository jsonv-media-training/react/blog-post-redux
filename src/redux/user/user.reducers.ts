import { Action, State, User } from '../../models';
import { GET_USER } from './user.type';

const initState: State<User[]> = {
  data: [],
  loading: false,
  error: ''
}

export const usersReducer = (state = initState, action: Action<User>): State<User[]> => {
  switch (action.type) {
    case GET_USER.START_REQUEST:
      return {
        ...state,
        loading: true
      };
    case GET_USER.REQUEST_SUCCESS:
      return {
        loading: false,
        data: [...state.data, action.payload as User],
        error: ''
      };
    case GET_USER.REQUEST_FAILURE:
      return {
        loading: false,
        data: [],
        error: action.payload as string
      };
    default:
      return state;
  }
}