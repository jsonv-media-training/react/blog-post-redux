import axios, { AxiosPromise } from 'axios';
import { from, Observable, throwError } from 'rxjs';

import { catchError, map } from 'rxjs/operators';
import { Image, Post, UnslashSearch, User } from '../models';

const URL = 'https://jsonplaceholder.typicode.com';
const URL_PHOTO = 'https://api.unsplash.com/search/photos';
const accessKey = '6U7zpsipQx9Qo02ltRQSnvwaFAdAS-U_dohria93EZY';

export const getPosts = (): Observable<Post[]> =>
  from<AxiosPromise<Post[]>>(axios.get(`${URL}/posts`, {}))
    .pipe(
      map(r => r.data),
      catchError(error => throwError(error.message))
    )

export const getImages = (length: number): Observable<Image[]> =>
  from<AxiosPromise<UnslashSearch>>(
    axios.get(URL_PHOTO,
      {
        headers: { Authorization: `Client-ID ${accessKey}` },
        params: { query: 'profile', orientation: 'squarish', page: 1, per_page: length }
    })
  ).pipe(
    map(r => r.data.results),
    catchError(error => throwError(error.message))
    );

export const getUser = (userId: number): Observable<User> =>
  from<AxiosPromise<User>>(axios.get(`${URL}/users/${userId}`, {}))
    .pipe(
      map(r => r.data),
      catchError(error => throwError(error.message))
    );
