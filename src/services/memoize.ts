import { shareReplay } from 'rxjs/operators';

export const memoize = (fn: any) => {
  let cache: any = {};
  // let cacheCreationDate = new Date().getTime();

  const memoizedFunction = (...args: any[]) => {

    // if (ageOfCacheInMilliseconds > 1000*60*60) {
    //   cache = {};
    //   cacheCreationDate = new Date().getTime();
    // }
    const cacheKey = JSON.stringify(args);

    if (typeof cache[cacheKey] === 'undefined') {
      const result = fn(...args).pipe(shareReplay());
      cache[cacheKey] = result;
      return result;
    }
    else {
      return cache[cacheKey];
    }
  }
  return memoizedFunction;
}